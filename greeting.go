package greeting

import (
	"math/rand"
	"time"
)

var (
	r         = rand.New(rand.NewSource(time.Now().UnixNano()))
	greetings = []string{
		"Hello",
		"Bonjour",
		"Hola",
		"Ciao",
		"Olà",
		"Namaste",
		"Salaam",
		"你好",
		"여보세요",
		"こんにちは",
	}
)

// Greet the user in a random language
func Greet(name string) string {
	var salutation = greetings[r.Intn(len(greetings))]
	return salutation + " " + name
}
